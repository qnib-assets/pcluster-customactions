#!/bin/bash

mkdir -p keys users
for x in $(seq -f %03g ${1});do
    uname="user$x"
    echo "Hey, I am '${uname}'"
    ssh-keygen -t rsa -b 4096 -N '' -f keys/$x-$uname
    mv keys/$x-$uname.pub users/$x-$uname.pub
done
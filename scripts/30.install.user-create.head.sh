#!/bin/bash
 
DIR_NAME=`dirname "$(readlink -f "$0")"`

# Make user dirs in /scratch
SCR_DIR=/shared/home
GROUP=spack
GID=5000

mkdir -p $SCR_DIR
if grep -q $GROUP /etc/group;then
    echo "group exists"
else
    groupadd --gid ${GID} ${GROUP}
fi
cp ${DIR_NAME}/resources/umask.sh /etc/profile.d/umask.sh
chmod +x /etc/profile.d/umask.sh

### ec2-user
usermod -aG spack ec2-user ||exit 0


for ufile in `find $DIR_NAME/users -type f -name '[0-9][0-9]*' -printf "%f\n" | sort -n `
do
    nuid=`echo $ufile | cut -d '-' -f 1`
    nuid=$(expr $nuid + 5000)

    nuname=`echo $ufile | cut -d '-' -f 2 |sed 's/\.pub//g'`

    adduser --base-dir ${SCR_DIR} -u ${nuid} -U -G ${GROUP},docker $nuname
    sudo su $nuname -c "ssh-keygen -t rsa -b 4096 -N '' -f ~/.ssh/id_rsa"
    sudo su $nuname -c "cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys"
    sudo su $nuname -c "chmod 600 ~/.ssh/authorized_keys"
        
	sudo su $nuname -c "chmod 750 ~/"
        cat $DIR_NAME/users/$ufile | sudo su $nuname -c "tee -a ~/.ssh/authorized_keys"

	# Setup dir and permissions
	chown -R $nuname:$nuname $SCR_DIR/$nuname
	chmod -R go-rwx $SCR_DIR/$nuname

	export SPACK_ROOT=/shared/spack
	echo "export SPACK_ROOT=$SPACK_ROOT" >> ${SCR_DIR}/$nuname/.bashrc
	echo "source $SPACK_ROOT/share/spack/setup-env.sh" >>  ${SCR_DIR}/$nuname/.bashrc
	chown $nuname:$nuname ${SCR_DIR}/$nuname/.bashrc
done
#!/bin/bash

# Copyright Christian Kniep. All Rights Reserved.
# SPDX-License-Identifier: MIT-0
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

curl -sL https://rpm.nodesource.com/setup_12.x | bash -

yum install -y nodejs
echo "u=rwx,g=rx,o=rx ~"
chmod u=rwx,g=rx,o=rx ~

echo "Installing Development Tools"
yum -y groupinstall "Development Tools"

echo "Intalling Cloud9"
wget -q https://dhj20r2nmszcd.cloudfront.net/static/c9-install-2.0.0.sh 
chmod +x c9-install-2.0.0.sh
sudo -H -u ec2-user bash -c ./c9-install-2.0.0.sh

echo -e "Cloud9 IDE@1\\nc9.ide.find@2\\nc9.ide.collab@1" |sudo -H -u ec2-user tee -a ~ec2-user/.c9/installed
#!/bin/bash

# Prevent non-system users from being able to access AWS resources

iptables -A OUTPUT --destination 169.254.169.254 -j REJECT
iptables -I OUTPUT --destination 169.254.169.254 -m owner --uid-owner root -j ACCEPT
iptables -I OUTPUT --destination 169.254.169.254 -m owner --uid-owner ec2-user -j ACCEPT
iptables -I OUTPUT --destination 169.254.169.254 -m owner --uid-owner slurm -j ACCEPT
#!/bin/bash

# Copyright Christian Kniep. All Rights Reserved.
# SPDX-License-Identifier: MIT-0
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

set -x

echo ">> install tools"
yum install -y squashfs-tools python3-pip
pip3 install j2cli[yaml]

export PATH="${PATH}:/usr/local/bin"

echo ">> Install SARUS"
mkdir -p /opt/sarus
cd /opt/sarus
wget -qO- https://github.com/eth-cscs/sarus/releases/download/1.4.2/sarus-Release.tar.gz |tar xfz - --strip-components=1
./configure_installation.sh
ln -s /opt/sarus/bin/sarus /usr/local/bin
ln -s /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem /etc/ssl/cert.pem

echo ">> Create jinja templates"
###################
export GL_URL=https://gitlab.com/qnib-assets/pcluster-customactions/-/raw/main/scripts/resources
### JINJA templates
mkdir -p /opt/jinja
mkdir -p /opt/sarus/etc/hooks.d
wget -qO /opt/jinja/sarus.json.j2 ${GL_URL}/sarus/1.4.2/sarus.json.j2
wget -qO /opt/jinja/timstamp-hook.json.j2 ${GL_URL}/sarus/timstamp-hook.json.j2

export GPU=false
export MPI_BIND_MOUNTS=""
export MPI_LIB_PATH=/opt/intel/mpi/latest/lib/release/libmpi.so.12
export SHARED_DIR=""


## MPI Libs
if [[ -f /opt/intel/mpi/latest/lib/release/libmpi.so.12 ]];then
    mkdir -p /opt/sarus/etc/hooks.d
    echo ">> Found /opt/intel/mpi/latest/lib/release/libmpi.so.12"
    GL_URL=https://gitlab.com/qnib-assets/pcluster-customactions/-/raw/main/scripts/resources
    if [[ -c /dev/infiniband/uverbs0 ]];then
        echo ">> Found /dev/infiniband/uverbs0"
        wget -qO /opt/sarus/etc/hooks.d/01-mpi-hook.json ${GL_URL}/sarus/1.4.2/pcluster/3.1.4/intemmpi-efa-hook.json

    else
        echo ">> No EFA found"
        wget -qO /opt/sarus/etc/hooks.d/01-mpi-hook.json ${GL_URL}/sarus/1.4.2/pcluster/3.1.4/intemmpi-hook.json
    fi
else
    if [[ -c /dev/infiniband/uverbs0 ]];then
        echo ">> Found /dev/infiniband/uverbs0"
        export MPI_BIND_MOUNTS="/dev/infiniband/uverbs0"
    fi
    export LDCONFIG_PATH=$(which ldconfig)
    j2 /opt/jinja/01-mpi-hook.json.j2 > /opt/sarus/etc/hooks.d/01-mpi-hook.json
fi

export EXTRA_MOUNTS=""
if [[ -d /fsx ]];then
    export EXTRA_MOUNTS="/fsx"
fi
if [[ -d /shared ]];then
    if [[ -z ${EXTRA_MOUNTS} ]];then
        export EXTRA_MOUNTS="/shared"
    else 
        export EXTRA_MOUNTS="${EXTRA_MOUNTS};/shared"
    fi
fi

j2 /opt/jinja/sarus.json.j2 > /opt/sarus/etc/sarus.json
export TIMESTAMP_HOOK_MESSAGE="After-runtime"
j2 /opt/jinja/timstamp-hook.json.j2 > /opt/sarus/etc/hooks.d/00-ts-runtime.json
export TIMESTAMP_HOOK_MESSAGE="After-MPI-hook"
j2 /opt/jinja/timstamp-hook.json.j2 > /opt/sarus/etc/hooks.d/02-ts-mpi-hook.json

nvidia-smi -q ||false
RC=$?
if [[ ${RC} == 0 ]];then
    echo ">> Found /proc/driver/nvidia/gpus/0 "
    wget -qO /opt/sarus/etc/hooks.d/03-nvidia-hook.json ${GL_URL}/sarus/1.4.2/nvidia-hook.json
fi

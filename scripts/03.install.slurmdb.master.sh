#!/bin/bash

# Copyright Christian Kniep. All Rights Reserved.
# SPDX-License-Identifier: MIT-0
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

DIR_NAME=`dirname "$(readlink -f "$0")"`

# Install MySQL (mariadb)

yum install -y mysql mariadb-server
systemctl start mariadb
systemctl enable mariadb
systemctl status mariadb

# Set-up DataBase fields

mysql -e "CREATE USER 'slurm'@'%' IDENTIFIED BY 'slurmaccouting';"
mysql -e "CREATE DATABASE slurm_acct_db;"
mysql -e "GRANT ALL ON slurm_acct_db.* TO 'slurm'@'%';"


HOST_SHORT=`hostname -s`

# Modify slurmdbd.conf

cat > /opt/slurm/etc/slurmdbd.conf << END
ArchiveEvents=yes
ArchiveJobs=yes
ArchiveResvs=yes
ArchiveSteps=no
ArchiveSuspend=no
ArchiveTXN=no
ArchiveUsage=no
AuthType=auth/munge
DbdHost=$HOST_SHORT
DbdPort=6819
DebugLevel=info
PurgeEventAfter=1month
PurgeJobAfter=12month
PurgeResvAfter=1month
PurgeStepAfter=1month
PurgeSuspendAfter=1month
PurgeTXNAfter=12month
PurgeUsageAfter=24month
SlurmUser=slurm
LogFile=/var/log/slurmdbd.log
PidFile=/var/run/slurmdbd.pid
StorageType=accounting_storage/mysql
StorageUser=slurm
StoragePass=slurmaccouting
StorageHost=$HOST_SHORT
StoragePort=3306
END


# Modify slurm.conf

cat >> /opt/slurm/etc/slurm.conf << END
#
AccountingStorageType=accounting_storage/slurmdbd
AccountingStorageHost=$HOST_SHORT
AccountingStorageUser=slurm
AccountingStoragePort=6819
END


# Ensure Files

touch /var/log/slurmdbd.log
touch /var/run/slurmdbd.pid

# Set permissions
chown slurm:slurm /opt/slurm/etc/slurmdbd.conf
chmod 600 /opt/slurm/etc/slurmdbd.conf

# Restart the services

cp ${DIR_NAME}/resources/slurmctld.service /etc/systemd/system/
cp ${DIR_NAME}/resources/slurmdbd.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable slurmdbd
systemctl start slurmdbd
sleep 2
systemctl restart slurmctld

/opt/slurm/bin/sacctmgr -i add cluster parallelcluster

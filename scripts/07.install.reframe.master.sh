#!/bin/bash

# Copyright Christian Kniep. All Rights Reserved.
# SPDX-License-Identifier: MIT-0
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

DIR_NAME=`dirname "$(readlink -f "$0")"`

RFM_ROOT=/shared/reframe

git clone https://github.com/eth-cscs/reframe.git $RFM_ROOT

echo "export RFM_ROOT=$RFM_ROOT" > /etc/profile.d/rfm.sh
echo "export PATH=$RFM_ROOT/bin:\$PATH" >> /etc/profile.d/rfm.sh

LH=`hostname -s`


# Instance type
ITYPE=$(curl --silent http://169.254.169.254/latest/meta-data/instance-type)
QUEUE="c6gn"
if [ $ITYPE = c5n.18xlarge ]
then
	QUEUE="c5n"
fi


cat > $RFM_ROOT/settings.py << EOF
site_configuration = {
    'systems': [
        {
            'name': 'aws',
            'descr': 'AWS PCluster',
            'hostnames': ['$LH'],
            'modules_system': 'spack',
            'partitions': [
                {
                    'name': 'login',
                    'descr': 'Login nodes',
                    'scheduler': 'local',
                    'launcher': 'local',
                    'environs': ['builtin'],
                },
                {
                    'name': '$QUEUE',
                    'descr': '$ITYPE Nodes',
                    'scheduler': 'slurm',
                    'launcher': 'srun',
                    'access': ['-p $QUEUE'],
                    'environs': ['builtin'],
                }
            ]
        }
    ],
    'environments': [
        {
            'name': 'builtin',
            'cc': 'gcc',
            'cxx': 'g++',
            'ftn': 'gfortran'
        },
    ],
    'logging': [
        {
            'level': 'debug',
            'handlers': [
                {
                    'type': 'stream',
                    'name': 'stdout',
                    'level': 'info',
                    'format': '%(message)s'
                },
                {
                    'type': 'file',
                    'level': 'debug',
                    'format': '[%(asctime)s] %(levelname)s: %(check_info)s: %(message)s',   # noqa: E501
                    'append': False
                }
            ],
            'handlers_perflog': [
                {
                    'type': 'filelog',
                    'prefix': '%(check_system)s/%(check_partition)s',
                    'level': 'info',
                    'format': (
                        '%(check_job_completion_time)s|reframe %(version)s|'
                        '%(check_info)s|jobid=%(check_jobid)s|'
                        '%(check_perf_var)s=%(check_perf_value)s|'
                        'ref=%(check_perf_ref)s '
                        '(l=%(check_perf_lower_thres)s, '
                        'u=%(check_perf_upper_thres)s)|'
                        '%(check_perf_unit)s'
                    ),
                    'append': True
                }
            ]
        }
    ],
}

EOF


# Make hackathon class

mkdir $RFM_ROOT/hackathon

cat > $RFM_ROOT/hackathon/__init__.py << EOF
import reframe as rfm
import reframe.utility.sanity as sn
import re
import subprocess
import json


class HackathonBase(rfm.RunOnlyRegressionTest, pin_prefix=True):
    valid_prog_environs = ['builtin']


    # Team Variables
    log_team_name=variable(str)
    log_app_name=variable(str)
    log_test_name=variable(str)


    # Ensure Required Team Variables
    @run_after('init')
    def _force_required(self):
        self.log_team_name
        self.log_app_name
        self.log_test_name

    # Extract Spack data
    @run_after('init')
    def extract_spack(self):
        out = subprocess.Popen(['spack', 'find', '--json'] + str(self.spec).split(' '), 
                stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        stdout, stderr = out.communicate()
        jsonS = stdout.decode('utf-8')
        spackdata = json.loads(jsonS)

        self.spack_app_name = spackdata[0]["name"]
        self.spack_app_hash = spackdata[0]["hash"]
        self.spack_app_ver = spackdata[0]["version"]

        self.spack_compiler_name = spackdata[0]["compiler"]["name"]
        self.spack_compiler_ver = spackdata[0]["compiler"]["version"]

    # Set Thread Data
    @run_before('run')
    def prepare_run(self):
        # Set Node
        self.num_tasks_per_node = int(self.parallelism['mpi']/self.parallelism['nodes'])

        # Set MPI
        self.num_tasks = self.parallelism['mpi']

        # Set OpenMP
        num_threads = self.parallelism['omp']
        self.num_cpus_per_task = num_threads
        self.variables['OMP_NUM_THREADS'] = str(num_threads)
        self.variables['OMP_PLACES'] = 'cores'

        self.modules.append(self.spec)

        self.name = self.log_app_name 
        self.name += '_' + self.log_test_name
        self.name += '_' + re.sub(r"[^a-z0-9]","_",self.spec.lower()) # Spack module
        self.name += '_N_' + str(self.parallelism['nodes'])
        self.name += '_MPI_' + str(self.parallelism['mpi'])
        self.name += '_OMP_' + str(self.parallelism['omp'])

EOF


# Set Permissions
chmod a+rwX -R $RFM_ROOT

# Install dependencies
cd $RFM_ROOT
./bootstrap.sh +pygelf
cd -

#!/bin/bash
 
DIR_NAME=`dirname "$(readlink -f "$0")"`

# Make user dirs in /scratch
SCR_DIR=/shared/home
GROUP=spack
GID=5000

mkdir -p $SCR_DIR
if grep -q $GROUP /etc/group;then
    echo "group exists"
else
    groupadd --gid ${GID} ${GROUP}
fi
cp ${DIR_NAME}/resources/umask.sh /etc/profile.d/umask.sh
chmod +x /etc/profile.d/umask.sh

### ec2-user
usermod -aG spack ec2-user ||exit 0


for ufile in `find $DIR_NAME/users -type f -name '[0-9][0-9]*' -printf "%f\n" | sort -n `
do
    nuid=`echo $ufile | cut -d '-' -f 1`
    nuid=$(expr $nuid + 5000)

    nuname=`echo $ufile | cut -d '-' -f 2 |sed 's/\.pub//g'`

    ## We just need to add the user, the home dir is created and filled on the head node
    adduser --base-dir ${SCR_DIR} -u ${nuid} --no-create-home -U -G ${GROUP},docker $nuname 
done
#!/bin/bash

# Copyright Christian Kniep. All Rights Reserved.
# SPDX-License-Identifier: MIT-0
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this
# software and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

DIR_NAME=`dirname "$(readlink -f "$0")"`

### Install Spack
# Install the latest Spack release
export SPACK_ROOT=/shared/spack
mkdir -p $SPACK_ROOT
cd $SPACK_ROOT/..
git clone https://github.com/spack/spack
cd $SPACK_ROOT
#git checkout releases/$( \
#    git branch -a --list '*releases*'| \
#    awk -F '/' 'END{print $NF}')
git checkout develop
source $SPACK_ROOT/share/spack/setup-env.sh

echo "export SPACK_ROOT=$SPACK_ROOT" > /etc/profile.d/spack.sh
echo "source $SPACK_ROOT/share/spack/setup-env.sh" >> /etc/profile.d/spack.sh

### Set Up Site Config
cp $DIR_NAME/resources/packages.yaml $SPACK_ROOT/etc/spack/packages.yaml
cp $DIR_NAME/resources/config.yaml $SPACK_ROOT/etc/spack/config.yaml
cp $DIR_NAME/resources/modules.yaml $SPACK_ROOT/etc/spack/
#### Compilers are installed via: install_missing_compilers: True
#spack install openmpi%arm
#spack install openmpi%gcc@10.3.0
#spack install openmpi%nvhpc

### Add SARUS repo
export SPACK_LOCAL_REPO=${SPACK_ROOT}/var/spack/repos/cscs
spack repo create ${SPACK_LOCAL_REPO}
spack repo add ${SPACK_LOCAL_REPO}
git clone https://github.com/eth-cscs/sarus.git /tmp/sarus
cp -r /tmp/sarus/spack/packages/* ${SPACK_LOCAL_REPO}/packages/
rm -rf /tmp/sarus

if [[ $(uname -i) == "x86_64" ]];then 
  echo "You are not running on an ARM instance, no need to do 'github.com/arm-hpc-user-group/arm_spack_extra'"
else 
  cd ${SPACK_ROOT}
  git clone https://github.com/arm-hpc-user-group/arm_spack_extra.git
  echo -e "repos:\n- ${SPACK_ROOT}/arm_spack_extra" > ${SPACK_ROOT}/etc/spack/repos.yaml
  echo -e "repos:\n- ${SPACK_ROOT}/arm_spack_extra" > ${SPACK_ROOT_CONTAINERS}/etc/spack/repos.yaml
fi

### Usersetup
mkdir -p ${SPACK_ROOT}/var
chown ec2-user: -R ${SPACK_ROOT}
